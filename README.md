# Tomcat9 Configuration for login.itlab.stanford.edu

There are three parts:

* `etc/tomcat9` is the actual Tomcat configuration
* `var/lib/tomcat9` includes the `ROOT` webapp
* `opt/cookieservlet` includes the X509 cookie / IdP logout servlet

## server.xml

All the configuration is in `etc/tomcat9/server.xml`. This Tomcat is configured to run
behind an Apache proxy, which is configured using the
[itlab/idp-apache-config](https://code.stanford.edu/et-public/itlab/idp-apache-config)
repo.

## Shibboleth IdP

The IdP runs at `https://login.itlab.stanford.edu/idp`, and is a normal-ish Shibboleth v4.x IdP.

